FROM php:7.4-fpm

RUN apt-get update \
    && cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini \
    && sed -i 's/memory_limit = 128M/memory_limit = -1/' /usr/local/etc/php/php.ini

RUN apt-get update -y && apt-get install -y \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libxpm-dev \
    libfreetype6-dev \
    libmcrypt-dev \
    libxml2-dev \
    libxslt-dev

RUN docker-php-ext-configure gd \
    --with-jpeg=/usr/lib/ \
    --with-jpeg=/usr/include/ \
    --with-freetype=/usr/include/

RUN docker-php-ext-install -j$(nproc) \
    lconv \
    mbstring \
    curl \
    openssl \
    tokenizer \
    xmlrpc \
    soap \
    ctype \
    zip \
    gd \
    simplexml \
    spl \
    pcre \
    dom \
    xml \
    json \
    && docker-php-ext-configure intl \
    && docker-php-ext-install -j$(nproc) intl

RUN docker-php-source delete \
    && apt-get remove -y g++ wget \
    && apt-get autoremove --purge -y && apt-get autoclean -y && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* /var/tmp/*

# Create dev user for specified uid:gid
ARG USER_ID=1000
ARG GROUP_ID=1000

RUN groupadd devdocker -g ${GROUP_ID} \
    && useradd -m -u ${USER_ID} -g ${GROUP_ID} dev

USER dev